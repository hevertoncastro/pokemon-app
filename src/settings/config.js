const config = {
  APP_TITLE: 'Pokémon Finder',
  API: {
    BASE_URL: 'https://vortigo.blob.core.windows.net/files/pokemon/data',
    TYPES: 'types.json',
    POKEMONS: 'pokemons.json',
  },
  PRELOAD: {
    BUTTON_TEXT: 'Start',
  },
  USER_INPUT: {
    PRIMARY_TEXT: "Let's meet",
    SECONDARY_TEXT: 'each other first?',
    BUTTON_TEXT: "Let's go",
    INPUT_PLACEHOLDER: 'Tell us your name',
  },
  CATEGORY_PICKER: {
    PRIMARY_TEXT: 'Hello, trainer',
    SECONDARY_TEXT: '...now tell us which is your favorite Pokémon type:',
  },
  FAVORITE: {
    TEXT: 'favorites',
    STORAGE: '@favorites_pokemons',
    ICON_URL: '../../assets/images/favorite.png',
  },
  POKEMONS_LIST: {
    CATEGORY_WIDTH: 116,
    CATEGORIES_FIT_IN_SCREEN: 3,
    SORT_ASC: 'asc',
    SORT_DESC: 'desc',
    SEARCH_PLACEHOLDER: 'Search a Pokémon...',
    SEARCH_FEEDBACK: 'Nenhum Pokémon encontrado, tente novamente.',
    LIMIT: 20,
    ON_END_THRESHOLD: 0.9,
  },
};

export default config;
