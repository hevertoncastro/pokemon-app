const colors = {
  black: '#1e1e1e',
  dark: '#262626',
  lightDark: '#595555',
  softDark: 'rgba(0, 0, 0, 0.85)',

  main: '#ffcb05',
  lightMain: '#ffda4b',
  darkMain: '#e5b500',

  white: '#fff',
  lightWhite: '#fafff6',
  darkWhite: '#f7f7f7',
  softWhite: 'rgba(255, 255, 255, 0.85)',

  gray: '#a9a9a9',
  silver: '#dcdcdc',
  softGray: '#fefefe',
  lightGray: '#f2f2f2',
  darkGray: '#969696',
  darkenGray: '#545454',

  alert: '#ff6e00',
  warning: '#f2ad3e',
  success: '#55b875',
  danger: '#d14b4b',

  transparent: 'transparent',

  muted: '',
};

export default colors;
