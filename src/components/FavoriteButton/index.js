import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { filterPokemons } from '../../actions/pokemons';
import AsyncStorage from '@react-native-community/async-storage';
import {
  addFavoritePokemon,
  removeFavoritePokemon,
} from '../../actions/pokemons';
import { FavoriteButtonHolder } from './styles';
import colors from '../../settings/colors';
import config from '../../settings/config';

import HeartIcon from '../../assets/icons/heart.svg';
import HeartFilledIcon from '../../assets/icons/heart-filled.svg';

export default ({ itemId }) => {
  const dispatch = useDispatch();
  const favoritePokemons = useSelector(
    (state) => state.pokemons.favoritePokemons,
  );

  const selectedCategory = useSelector(
    (state) => state.categories.selectedCategory,
  );

  const isFavorite = (id) => {
    return favoritePokemons?.indexOf(id) > -1;
  };

  const addFavoriteInStorage = async (id) => {
    const updatedFavorites = [...favoritePokemons];
    updatedFavorites.push(id);
    try {
      const jsonValue = JSON.stringify(updatedFavorites);
      await AsyncStorage.setItem(config.FAVORITE.STORAGE, jsonValue);
    } catch (_) {}
  };

  const removeFavoriteFromStorage = async (id) => {
    const filtered = favoritePokemons.filter((item) => item !== id);

    try {
      const jsonValue = JSON.stringify(filtered);
      await AsyncStorage.setItem(config.FAVORITE.STORAGE, jsonValue);
    } catch (_) {}
  };

  const handleFavoritePress = (id) => {
    if (isFavorite(id)) {
      dispatch(removeFavoritePokemon(id));
      removeFavoriteFromStorage(id);
      dispatch(filterPokemons(selectedCategory));
    } else {
      dispatch(addFavoritePokemon(id));
      addFavoriteInStorage(id);
    }
  };

  return (
    <FavoriteButtonHolder onPress={() => handleFavoritePress(itemId)}>
      {isFavorite(itemId) ? (
        <HeartFilledIcon width="100%" height="32" fill={colors.danger} />
      ) : (
        <HeartIcon width="100%" height="32" fill={colors.danger} />
      )}
    </FavoriteButtonHolder>
  );
};
