import styled from 'styled-components/native';
import colors from '../../settings/colors';

export const ListItemHolder = styled.View`
  width: 100%;
  height: 110px;
  min-height: 110px;
  flex-direction: row;
  justify-content: flex-start;
  align-items: flex-start;
`;

export const ListItemContent = styled.TouchableOpacity`
  width: 80%;
  min-height: 110px;
  flex-direction: column;
  justify-content: flex-start;
  align-items: flex-start;
  padding-top: 12px;
  padding-bottom: 12px;
  padding-right: 16px;
  padding-left: 16px;
  background-color: ${colors.dark};

  ${({ alternated }) =>
    alternated &&
    `
    background-color: ${colors.black};
  `}
`;

export const ListItemActions = styled.View`
  width: 20%;
  height: 100%;
  justify-content: center;
  align-items: center;
  border-style: solid;
  border-left-width: 1px;
  border-left-color: ${colors.lightDark};
  background-color: ${colors.dark};

  ${({ alternated }) =>
    alternated &&
    `
    background-color: ${colors.black};
  `}
`;

export const ListItemContentMain = styled.TouchableOpacity`
  width: 80%;
  flex-direction: row;
  justify-content: flex-start;
  align-items: center;
`;

export const ListItemImageHolder = styled.View`
  width: 88px;
  height: 88px;
  margin-right: 12px;
  /* background-color: ${colors.dark}; */
  border-radius: 24px;
`;

export const ListItemImage = styled.Image`
  width: 88px;
  height: 88px;
  border-radius: 24px;
`;

export const ListItemTitleHolder = styled.View`
  align-items: flex-start;
  flex-direction: column;
  justify-content: flex-start;
  width: 100%;
`;

export const ListItemTitle = styled.Text`
  font-size: 18px;
  color: ${colors.white};
`;

export const ListItemContentAside = styled.View`
  align-items: flex-start;
  flex-direction: row;
  justify-content: flex-start;
  width: 100%;
  padding-top: 12px;
  padding-bottom: 8px;
`;

export const ListItemTag = styled.Text`
  font-size: 12px;
  color: ${colors.white};
  border-width: 1px;
  border-style: solid;
  border-color: ${colors.white};
  padding-top: 4px;
  padding-bottom: 4px;
  padding-left: 8px;
  padding-right: 8px;
  border-radius: 12px;
  margin-right: 8px;
`;
