import React from 'react';
import { useNavigation } from '@react-navigation/native';

import {
  ListItemHolder,
  ListItemContent,
  ListItemContentMain,
  ListItemImageHolder,
  ListItemImage,
  ListItemTitleHolder,
  ListItemTitle,
  ListItemContentAside,
  ListItemTag,
  ListItemActions,
} from './styles';

import FavoriteButton from '../FavoriteButton';

const ListItem = ({ item, index }) => {
  const navigation = useNavigation();
  const { id, name, thumbnailImage, abilities } = item;

  const handleItemPress = () => {
    navigation.navigate('PokemonDetails', {
      pokemon: item,
    });
  };

  const renderPokemonAbilities = () => {
    return abilities.map((ability, abilityIndex) => (
      <ListItemTag key={abilityIndex}>{ability}</ListItemTag>
    ));
  };

  return (
    <ListItemHolder>
      <ListItemContent alternated={index % 2}>
        <ListItemContentMain onPress={handleItemPress}>
          <ListItemImageHolder>
            {!!thumbnailImage && (
              <ListItemImage
                source={{ uri: thumbnailImage }}
                resizeMode="cover"
              />
            )}
          </ListItemImageHolder>
          <ListItemTitleHolder>
            <ListItemTitle size={2} numberOfLines={2}>
              {name}
            </ListItemTitle>
            <ListItemContentAside>
              {renderPokemonAbilities()}
            </ListItemContentAside>
          </ListItemTitleHolder>
        </ListItemContentMain>
      </ListItemContent>

      <ListItemActions alternated={index % 2}>
        <FavoriteButton itemId={id} />
      </ListItemActions>
    </ListItemHolder>
  );
};

export default ListItem;
