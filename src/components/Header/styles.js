import styled from 'styled-components/native';
import colors from '../../settings/colors';

export const HeaderHolder = styled.View`
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  height: 50px;
  border-bottom-width: 1px;
  border-style: solid;
  background-color: ${colors.dark};
  border-bottom-color: ${colors.softDark};
`;

export const HeaderTitleHolder = styled.View`
  flex: 1;
  justify-content: center;
  align-items: center;
  height: 100%;
`;

export const HeaderTitle = styled.Text`
  font-size: 18px;
  color: ${colors.white};
`;

export const HeaderIconHolder = styled.View`
  flex: 0.2;
  justify-content: center;
  align-items: center;
  height: 100%;
`;

export const HeaderRightSlot = styled.View`
  flex: 0.2;
  justify-content: center;
  align-items: flex-end;
  height: 100%;
`;

export const HeaderButton = styled.TouchableHighlight`
  flex: 1;
  align-items: center;
  justify-content: center;
  width: 100%;
  height: 100%;
`;
