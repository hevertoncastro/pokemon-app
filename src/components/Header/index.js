import React from 'react';
import FavoriteButton from '../FavoriteButton';
import {
  HeaderHolder,
  HeaderIconHolder,
  HeaderTitleHolder,
  HeaderTitle,
  HeaderRightSlot,
  HeaderButton,
} from './styles';

import BackIcon from '../../assets/icons/back.svg';
import SortIcon from '../../assets/icons/sort.svg';
import SearchIcon from '../../assets/icons/search.svg';
import CloseIcon from '../../assets/icons/close.svg';
import colors from '../../settings/colors';

const CustomHeader = ({
  title = false,
  back = null,
  sort = null,
  sortState = true,
  search = null,
  searchState = false,
  pokemonId = null,
}) => {
  return (
    <HeaderHolder>
      <HeaderIconHolder>
        {!!back && (
          <HeaderButton onPress={back}>
            <BackIcon width="100%" height="32" fill={colors.danger} />
          </HeaderButton>
        )}
        {!!sort && (
          <HeaderButton onPress={sort}>
            <SortIcon
              width="100%"
              height="24"
              fill={sortState ? colors.white : colors.darkenGray}
            />
          </HeaderButton>
        )}
      </HeaderIconHolder>

      <HeaderTitleHolder>
        {title && <HeaderTitle>{title}</HeaderTitle>}
      </HeaderTitleHolder>

      <HeaderRightSlot>
        {pokemonId && <FavoriteButton itemId={pokemonId} />}

        {!!search && (
          <HeaderButton onPress={search}>
            {searchState ? (
              <CloseIcon width="100%" height="20" fill={colors.white} />
            ) : (
              <SearchIcon width="100%" height="24" fill={colors.white} />
            )}
          </HeaderButton>
        )}
      </HeaderRightSlot>
    </HeaderHolder>
  );
};

export default CustomHeader;
