import styled from 'styled-components/native';
import colors from '../../settings/colors';

export const ButtonsHolder = styled.View`
  flex-direction: row;
  justify-content: space-between;
  width: 180px;
  margin-top: 16px;
  margin-bottom: 16px;
`;

export const Button = styled.TouchableOpacity`
  flex: 1;
  align-items: center;
  justify-content: center;
  border-radius: 30px;
  height: 60px;
  width: 100%;
  background-color: ${colors.transparent};
  border-width: 2px;
  border-style: solid;
  border-color: ${colors.main};
`;

export const ButtonText = styled.Text`
  font-size: 18px;
  color: ${colors.main};
`;
