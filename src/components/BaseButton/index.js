import React from 'react';
import { ButtonsHolder, Button, ButtonText } from './styles';

const BaseButton = ({ onPress, text }) => {
  return (
    <ButtonsHolder>
      <Button onPress={onPress} activeOpacity={0.8}>
        <ButtonText primary>{text}</ButtonText>
      </Button>
    </ButtonsHolder>
  );
};

export default BaseButton;
