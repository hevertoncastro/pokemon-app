import styled from 'styled-components/native';
import colors from '../../settings/colors';

export const InputHolder = styled.View`
  width: 90%;
  height: 60px;
  background-color: ${colors.white};
  flex-direction: row;
  border-radius: 30px;
  padding-left: 15px;
  align-items: center;
  margin-bottom: 15px;
`;

export const Input = styled.TextInput`
  flex: 3.5;
  font-size: 16px;
  padding: 8px;
`;

export const SearchHolder = styled.TouchableHighlight`
  flex: 1;
  justify-content: center;
  align-items: flex-end;
  height: 100%;
`;
