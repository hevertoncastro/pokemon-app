import styled from 'styled-components/native';
import colors from '../../settings/colors';

export const CategoriesListHolder = {
  flex: 1,
  width: '100%',
  backgroundColor: `${colors.white}`,
};

export const CategoriesListButton = styled.TouchableOpacity`
  flex-direction: column;
  justify-content: center;
  align-items: center;
  width: 100px;
  height: 130px;
  margin-left: 16px;
  padding-top: 24px;
  padding-bottom: 12px;
  border-radius: 50px;

  ${({ alternated }) =>
    alternated &&
    `
    background-color: ${colors.black};
  `}
`;

export const CategoriesListImageHolder = styled.View`
  flex-direction: column;
  justify-content: center;
  align-items: center;
  width: 96px;
  height: 96px;
  background-color: ${colors.dark};
  border-radius: 50px;

  ${({ active }) =>
    active &&
    `
    background-color: ${colors.danger};
  `}
`;

export const CategoriesListImage = styled.Image`
  width: 90px;
  height: 90px;
  border-radius: 50px;
  border-width: 5px;
  background-color: ${colors.dark};
  border-color: ${colors.dark};
`;

export const CategoriesListTitle = styled.Text`
  font-size: 18px;
  color: ${colors.gray};
  font-weight: normal;
  margin-top: 4px;

  ${({ active }) =>
    active &&
    `
    color: ${colors.white};
    font-weight: bold;
  `}
`;
