import React from 'react';
import { Platform, ScrollView } from 'react-native';
import { useSelector, useDispatch } from 'react-redux';
import { selectCategory } from '../../actions/categories';
import { filterPokemons } from '../../actions/pokemons';

import {
  CategoriesListHolder,
  CategoriesListButton,
  CategoriesListImageHolder,
  CategoriesListImage,
  CategoriesListTitle,
} from './styles';

import config from '../../settings/config';

const CategoriesList = () => {
  const dispatch = useDispatch();

  const categories = useSelector((state) => state.categories.categories);
  const selectedCategory = useSelector(
    (state) => state.categories.selectedCategory,
  );

  const handleCategorySelect = (categoryName) => {
    dispatch(selectCategory(categoryName));
    dispatch(filterPokemons(categoryName));
  };

  const scrollToSelectedCategory = () => {
    if (Platform.OS !== 'ios') {
      return 0;
    }

    const scrollLimit =
      categories.length - config.POKEMONS_LIST.CATEGORIES_FIT_IN_SCREEN;

    const getSelectedCategoryIndex = categories.findIndex(
      (category) => category.name === selectedCategory,
    );

    const categoriesMultiplyLimit =
      getSelectedCategoryIndex > scrollLimit
        ? scrollLimit
        : getSelectedCategoryIndex;

    return getSelectedCategoryIndex > -1
      ? categoriesMultiplyLimit * config.POKEMONS_LIST.CATEGORY_WIDTH
      : 0;
  };

  const renderPokemonCategories = () => {
    return categories.map((category, categoryIndex) => (
      <CategoriesListButton
        key={`${categoryIndex}:${category.name}`}
        onPress={() => handleCategorySelect(category.name)}>
        <CategoriesListImageHolder active={selectedCategory === category.name}>
          {category.name === config.FAVORITE.TEXT ? (
            <CategoriesListImage
              source={require('../../assets/images/favorite.png')}
              resizeMode="cover"
            />
          ) : (
            <CategoriesListImage
              source={{ uri: category.thumbnailImage }}
              resizeMode="cover"
            />
          )}
        </CategoriesListImageHolder>
        <CategoriesListTitle active={selectedCategory === category.name}>
          {category.name}
        </CategoriesListTitle>
      </CategoriesListButton>
    ));
  };

  return (
    <ScrollView
      horizontal
      showsHorizontalScrollIndicator={false}
      contentOffset={{ x: scrollToSelectedCategory() }}
      styles={CategoriesListHolder}>
      {renderPokemonCategories()}
    </ScrollView>
  );
};

export default CategoriesList;
