import React from 'react';
import { StatusBar } from 'react-native';
import colors from '../../settings/colors';

const Status = ({ theme = 'dark', hidden }) => {
  return (
    <StatusBar
      backgroundColor={themes[theme].background}
      barStyle={themes[theme].style}
      hidden={hidden}
    />
  );
};

const themes = {
  light: {
    background: `${colors.white}`,
    style: 'dark-content',
  },
  dark: {
    background: `${colors.dark}`,
    style: 'light-content',
  },
  coffee: {
    background: `${colors.coffee}`,
    style: 'light-content',
  },
};

export default Status;
