export const sortList = (key, order = 'asc') => {
  return function (a, b) {
    if (order === 'desc') {
      return b[key] > a[key] ? 1 : b[key] < a[key] ? -1 : 0;
    }

    return b[key] < a[key] ? 1 : b[key] > a[key] ? -1 : 0;
  };
};
