import styled from 'styled-components/native';
import colors from '../../settings/colors';

export const Container = styled.SafeAreaView`
  background-color: ${colors.dark};
  flex: 1;
  justify-content: flex-start;
  align-items: center;
`;

export const PokemonDetailsHolder = styled.View`
  width: 100%;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  padding-top: 24px;
  padding-left: 24px;
  padding-right: 24px;
`;
export const PokemonHeaderHolder = styled.View`
  width: 100%;
  flex-direction: row;
  justify-content: flex-start;
  align-items: flex-start;
  margin-top: 24px;
`;

export const PokemonHeaderTextsHolder = styled.View`
  flex: 1;
  flex-direction: column;
  justify-content: center;
  align-items: flex-start;
  margin-left: 20px;
`;

export const PokemonIconTextHolder = styled.View`
  /* width: 100px; */
  flex-direction: row;
  justify-content: flex-start;
  align-items: center;
  margin-top: 16px;
`;

export const PokemonIconText = styled.Text`
  font-size: 18px;
  color: ${colors.white};
  margin-left: 8px;
`;

export const PokemonDetailsImage = styled.Image`
  flex: 1;
  width: 215px;
  height: 215px;
  border-radius: 24px;
  background-color: ${colors.dark};
`;

export const PokemonDetailsTitleHolder = styled.View`
  align-items: flex-start;
  flex-direction: row;
  justify-content: flex-start;
  width: 100%;
`;

export const PokemonDetailsTitle = styled.Text`
  font-size: 26px;
  color: ${colors.white};
  font-weight: bold;
  margin-top: 24px;
  margin-bottom: 16px;
`;

export const PokemonDetailsText = styled.Text`
  font-size: 18px;
  color: ${colors.white};
  line-height: 36px;
  text-transform: capitalize;

  ${({ bold }) =>
    bold &&
    `
    font-weight: bold;
  `}
`;

export const PokemonDetailsTabsHolder = styled.View`
  flex-direction: row;
  align-items: flex-end;
  justify-content: flex-start;
  width: 100%;
  margin-top: 24px;
  /* padding-bottom: 8px; */
`;

export const PokemonDetailsTab = styled.TouchableOpacity`
  border-width: 1px;
  border-style: solid;
  border-color: ${colors.lightWhite};
  color: ${colors.lightWhite};
  padding-top: 4px;
  padding-bottom: 4px;
  padding-left: 16px;
  padding-right: 16px;
  margin-top: 8px;
  margin-right: 2px;
  border-top-right-radius: 8px;
  border-top-left-radius: 8px;

  ${({ active }) =>
    active &&
    `
    border-color: ${colors.lightWhite};
    background-color: ${colors.lightWhite};
    padding-top: 6px;
    padding-bottom: 6px;
  `}
`;

export const PokemonDetailsTabText = styled.Text`
  font-size: 16px;
  color: ${colors.lightWhite};

  ${({ active }) =>
    active &&
    `
    color: ${colors.dark};
  `}
`;

export const PokemonDetailsContentHolder = styled.View`
  justify-content: flex-start;
  width: 100%;
  border-width: 1px;
  border-style: solid;
  border-color: ${colors.lightWhite};
  padding: 12px 24px;
  border-top-right-radius: 15px;
  border-bottom-right-radius: 15px;
  border-bottom-left-radius: 15px;
`;
