import React, { useState } from 'react';
import { ScrollView } from 'react-native';
import { Container } from './styles';
import StatusBar from '../../components/StatusBar';
import Header from '../../components/Header';
import ContentHolder from '../../components/ContentHolder';
import {
  PokemonDetailsHolder,
  PokemonHeaderHolder,
  PokemonHeaderTextsHolder,
  PokemonIconTextHolder,
  PokemonIconText,
  PokemonDetailsImage,
  PokemonDetailsTitle,
  PokemonDetailsText,
  PokemonDetailsTabsHolder,
  PokemonDetailsTab,
  PokemonDetailsTabText,
  PokemonDetailsContentHolder,
} from './styles';
import colors from '../../settings/colors';
import HeightIcon from '../../assets/icons/height.svg';
import WeightIcon from '../../assets/icons/weight.svg';

const PokemonDetails = ({ route, navigation }) => {
  const { pokemon } = route.params;
  const [selectedTab, setSelectedTab] = useState('abilities');

  return (
    <Container>
      <StatusBar />
      <Header
        title={pokemon.name}
        back={() => navigation.goBack()}
        pokemonId={pokemon.id}
      />
      <ScrollView style={ContentHolder}>
        <PokemonDetailsHolder>
          <PokemonHeaderHolder>
            {!!pokemon.thumbnailImage && (
              <PokemonDetailsImage
                source={{ uri: pokemon.thumbnailImage }}
                accessibilityLabel={pokemon.thumbnailAltText}
                accessible
                resizeMode="cover"
              />
            )}
            <PokemonHeaderTextsHolder>
              {!!pokemon.name && (
                <PokemonDetailsTitle>{pokemon.name}</PokemonDetailsTitle>
              )}

              <PokemonIconTextHolder>
                <HeightIcon width="28" height="28" fill={colors.white} />
                <PokemonIconText>Height: {pokemon.height} cm</PokemonIconText>
              </PokemonIconTextHolder>
              <PokemonIconTextHolder>
                <WeightIcon width="28" height="28" fill={colors.white} />
                <PokemonIconText>Weight: {pokemon.weight} kg</PokemonIconText>
              </PokemonIconTextHolder>
            </PokemonHeaderTextsHolder>
          </PokemonHeaderHolder>
          <PokemonDetailsTabsHolder>
            {!!pokemon.abilities && (
              <PokemonDetailsTab
                active={selectedTab === 'abilities'}
                onPress={() => setSelectedTab('abilities')}>
                <PokemonDetailsTabText active={selectedTab === 'abilities'}>
                  Abilities
                </PokemonDetailsTabText>
              </PokemonDetailsTab>
            )}
            {!!pokemon.weakness && (
              <PokemonDetailsTab
                active={selectedTab === 'weakness'}
                onPress={() => setSelectedTab('weakness')}>
                <PokemonDetailsTabText active={selectedTab === 'weakness'}>
                  Weakness
                </PokemonDetailsTabText>
              </PokemonDetailsTab>
            )}
            {!!pokemon.type && (
              <PokemonDetailsTab
                active={selectedTab === 'type'}
                onPress={() => setSelectedTab('type')}>
                <PokemonDetailsTabText active={selectedTab === 'type'}>
                  Type{pokemon.type?.length > 1 ? 's' : ''}
                </PokemonDetailsTabText>
              </PokemonDetailsTab>
            )}
          </PokemonDetailsTabsHolder>

          {(pokemon.abilities?.length > 0 ||
            pokemon.weakness?.length > 0 ||
            pokemon.type?.length > 0) && (
            <PokemonDetailsContentHolder>
              {pokemon.abilities?.length > 0 &&
                selectedTab === 'abilities' &&
                pokemon.abilities?.map((item, index) => (
                  <PokemonDetailsText key={index}>{item}</PokemonDetailsText>
                ))}
              {pokemon.weakness?.length > 0 &&
                selectedTab === 'weakness' &&
                pokemon.weakness?.map((item, index) => (
                  <PokemonDetailsText key={index}>{item}</PokemonDetailsText>
                ))}
              {pokemon.type?.length > 0 &&
                selectedTab === 'type' &&
                pokemon.type?.map((item, index) => (
                  <PokemonDetailsText key={index}>{item}</PokemonDetailsText>
                ))}
            </PokemonDetailsContentHolder>
          )}
        </PokemonDetailsHolder>
      </ScrollView>
    </Container>
  );
};

export default PokemonDetails;
