import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import {
  searchPokemons,
  sortPokemons,
  filterPokemons,
} from '../../actions/pokemons';
import { FlatList } from 'react-native';
import { Container } from './styles';
import StatusBar from '../../components/StatusBar';
import Header from '../../components/Header';
import ListItem from '../../components/ListItem';
import CategoriesList from '../../components/CategoriesList';
import InputHolder from '../../components/InputHolder';
import {
  PokemonsListCategoriesHolder,
  PokemonsListFilters,
  PokemonsListFilterFeedback,
} from './styles';
import config from '../../settings/config';

const PokemonsList = () => {
  const dispatch = useDispatch();

  const selectedCategory = useSelector(
    (state) => state.categories.selectedCategory,
  );

  const filteredPokemons = useSelector(
    (state) => state.pokemons.filteredPokemons,
  );
  const sortOrder = useSelector((state) => state.pokemons.sortOrder);

  const [localSearch, setLocalSearch] = useState('');
  const [searchVisibility, setSearchVisibility] = useState(false);

  const renderItem = ({ item, index }) => (
    <ListItem item={item} index={index} />
  );

  const handleSearch = () => {
    dispatch(filterPokemons(selectedCategory));
    dispatch(searchPokemons(localSearch));
  };

  const handleVisibility = () => {
    if (!searchVisibility) {
      setSearchVisibility(true);
    } else {
      setLocalSearch('');
      setSearchVisibility(false);
      dispatch(searchPokemons(''));
      dispatch(filterPokemons(selectedCategory));
    }
  };

  const handleSort = () => {
    const order =
      sortOrder === config.POKEMONS_LIST.SORT_ASC
        ? config.POKEMONS_LIST.SORT_DESC
        : config.POKEMONS_LIST.SORT_ASC;
    dispatch(sortPokemons(order));
  };

  return (
    <Container>
      <StatusBar />
      <Header
        title={config.APP_TITLE}
        sort={handleSort}
        sortState={sortOrder === config.POKEMONS_LIST.SORT_ASC}
        search={handleVisibility}
        searchState={searchVisibility}
      />

      {searchVisibility && (
        <PokemonsListFilters>
          <InputHolder
            onChangeText={(searchQuery) => setLocalSearch(searchQuery)}
            placeholder={config.POKEMONS_LIST.SEARCH_PLACEHOLDER}
            onSearch={handleSearch}
          />
        </PokemonsListFilters>
      )}

      <PokemonsListCategoriesHolder>
        <CategoriesList />
      </PokemonsListCategoriesHolder>

      {filteredPokemons.length === 0 && (
        <PokemonsListFilterFeedback>
          {config.POKEMONS_LIST.SEARCH_FEEDBACK}
        </PokemonsListFilterFeedback>
      )}

      <FlatList
        data={filteredPokemons}
        renderItem={renderItem}
        keyExtractor={(pokemon) => pokemon.id.toString()}
        onEndReachedThreshold={0.9}
        initialNumToRender={20}
      />
    </Container>
  );
};

export default PokemonsList;
