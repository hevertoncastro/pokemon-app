import React from 'react';
import styled from 'styled-components/native';
import colors from '../../settings/colors';

export const Container = styled.SafeAreaView`
  background-color: ${colors.dark};
  flex: 1;
  justify-content: flex-start;
  align-items: center;
`;

export const CategoryPickerHolder = styled.View`
  width: 100%;
  height: 60%;
`;

export const CategoryPickerWrap = styled.View`
  width: 100%;
  flex-direction: row;
  flex-wrap: wrap;
  justify-content: center;
  align-items: center;
`;

export const PrimaryTextHolder = styled.View`
  flex-direction: column;
  justify-content: flex-start;
  margin-top: 40px;
  margin-bottom: 20px;
  width: 80%;
`;

export const PrimaryText = styled.Text`
  font-size: 42px;
  font-weight: bold;
  color: ${colors.main};
`;

export const SecondaryText = styled.Text`
  font-size: 24px;
  color: ${colors.lightMain};
  margin-top: 20px;
`;
