import React from 'react';
import { ScrollView } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { useSelector, useDispatch } from 'react-redux';
import { selectCategory } from '../../actions/categories';
import { filterPokemons } from '../../actions/pokemons';
import StatusBar from '../../components/StatusBar';
import BaseButton from '../../components/BaseButton';
import ContentHolder from '../../components/ContentHolder';
import {
  Container,
  CategoryPickerHolder,
  CategoryPickerWrap,
  PrimaryTextHolder,
  PrimaryText,
  SecondaryText,
} from './styles';

import {
  CategoriesListButton,
  CategoriesListImageHolder,
  CategoriesListImage,
  CategoriesListTitle,
} from '../../components/CategoriesList/styles';
import config from '../../settings/config';

export default () => {
  const dispatch = useDispatch();
  const navigation = useNavigation();
  const userName = useSelector((state) => state.user.userName);
  const categories = useSelector((state) => state.categories.categories);

  const handleCategoryPress = (categoryName) => {
    dispatch(selectCategory(categoryName));
    dispatch(filterPokemons(categoryName));

    navigation.reset({
      routes: [
        {
          name: 'PokemonsList',
        },
      ],
    });
  };

  const renderPokemonCategories = () => {
    const categoriesWithoutFavorite = categories.filter(
      (category) => category.name !== config.FAVORITE.TEXT,
    );
    return categoriesWithoutFavorite.map((category, categoryIndex) => (
      <CategoriesListButton
        key={`${categoryIndex}:${category.name}`}
        onPress={() => handleCategoryPress(category.name)}>
        <CategoriesListImageHolder>
          <CategoriesListImage
            source={{ uri: category.thumbnailImage }}
            resizeMode="cover"
          />
        </CategoriesListImageHolder>
        <CategoriesListTitle>{category.name}</CategoriesListTitle>
      </CategoriesListButton>
    ));
  };

  return (
    <Container>
      <StatusBar />
      <PrimaryTextHolder>
        <PrimaryText>{config.CATEGORY_PICKER.PRIMARY_TEXT}</PrimaryText>
        {userName && <PrimaryText>{userName}!</PrimaryText>}

        <SecondaryText>{config.CATEGORY_PICKER.SECONDARY_TEXT}</SecondaryText>
      </PrimaryTextHolder>
      <CategoryPickerHolder>
        <ScrollView style={ContentHolder}>
          <CategoryPickerWrap>{renderPokemonCategories()}</CategoryPickerWrap>
        </ScrollView>
      </CategoryPickerHolder>
    </Container>
  );
};
