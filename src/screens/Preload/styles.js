import React from 'react';
import styled from 'styled-components/native';
import colors from '../../settings/colors';

export const Container = styled.SafeAreaView`
  background-color: ${colors.dark};
  flex: 1;
  justify-content: center;
  align-items: center;
`;

export const PokemonLogo = styled.Image`
  width: 100%;
  height: 120px;
`;

export const FinderImage = styled.Image`
  width: 100%;
  height: 80px;
  margin-bottom: 50px;
`;
