import React, { useEffect } from 'react';
import { useNavigation } from '@react-navigation/native';
import { useDispatch } from 'react-redux';
import { setFavoritePokemons } from '../../actions/pokemons';
import AsyncStorage from '@react-native-community/async-storage';
import StatusBar from '../../components/StatusBar';
import BaseButton from '../../components/BaseButton';
import { Container, PokemonLogo, FinderImage } from './styles';
import config from '../../settings/config';

const PreLoad = () => {
  const dispatch = useDispatch();
  const navigation = useNavigation();

  useEffect(() => {
    AsyncStorage.getItem(config.FAVORITE.STORAGE).then((pokemons) => {
      const favoritePokemons = pokemons != null ? JSON.parse(pokemons) : [];
      dispatch(setFavoritePokemons(favoritePokemons));
    });
  }, [dispatch, navigation]);

  const handleStartPress = () => {
    navigation.reset({
      routes: [{ name: 'UserInput' }],
    });
  };

  return (
    <Container>
      <StatusBar />
      <PokemonLogo
        source={require('../../assets/images/pokemon-logo.png')}
        resizeMode="contain"
        accessibilityLabel={config.APP_TITLE}
        accessible
      />
      <FinderImage
        source={require('../../assets/images/finder.png')}
        resizeMode="contain"
      />
      <BaseButton
        onPress={handleStartPress}
        text={config.PRELOAD.BUTTON_TEXT}
      />
    </Container>
  );
};

export default PreLoad;
