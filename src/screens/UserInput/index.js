import React, { useEffect, useState } from 'react';
import { useNavigation } from '@react-navigation/native';
import { useDispatch } from 'react-redux';
import { setUserName } from '../../actions/user';
import { fetchCategories } from '../../actions/categories';
import { fetchPokemons } from '../../actions/pokemons';
import {
  Container,
  PrimaryTextHolder,
  PrimaryText,
  PikachuImage,
} from './styles';
import StatusBar from '../../components/StatusBar';
import BaseButton from '../../components/BaseButton';
import InputHolder from '../../components/InputHolder';
import config from '../../settings/config';

const UserInput = () => {
  const dispatch = useDispatch();
  const navigation = useNavigation();

  useEffect(() => {
    dispatch(fetchCategories());
    dispatch(fetchPokemons());
  });

  const handleStartPress = (event) => {
    navigation.reset({
      routes: [{ name: 'CategoryPicker' }],
    });
  };

  return (
    <Container>
      <StatusBar />

      <PrimaryTextHolder>
        <PrimaryText>{config.USER_INPUT.PRIMARY_TEXT}</PrimaryText>
        <PrimaryText>{config.USER_INPUT.SECONDARY_TEXT}</PrimaryText>
      </PrimaryTextHolder>

      <PikachuImage
        source={require('../../assets/images/pikachu.png')}
        resizeMode="cover"
      />

      <InputHolder
        onChangeText={(text) => dispatch(setUserName(text))}
        placeholder={config.USER_INPUT.INPUT_PLACEHOLDER}
      />

      <BaseButton
        onPress={handleStartPress}
        text={config.USER_INPUT.BUTTON_TEXT}
      />
    </Container>
  );
};

export default UserInput;
