import React from 'react';
import styled from 'styled-components/native';
import colors from '../../settings/colors';

export const Container = styled.SafeAreaView`
  background-color: ${colors.dark};
  flex: 1;
  justify-content: center;
  align-items: center;
`;

export const PrimaryTextHolder = styled.View`
  flex-direction: column;
  margin-bottom: 40px;
`;

export const PrimaryText = styled.Text`
  font-size: 42px;
  font-weight: bold;
  color: ${colors.main};
`;

export const PikachuImage = styled.Image`
  position: absolute;
  bottom: 0;
  right: 0;
  width: 194px;
  height: 202px;
`;
