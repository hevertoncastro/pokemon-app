import thunk from 'redux-thunk';
import { createStore, applyMiddleware, combineReducers } from 'redux';

// Reducers
import userReducer from './reducers/userReducer';
import pokemonsReducer from './reducers/pokemonsReducer';
import categoriesReducer from './reducers/categoriesReducer';

// Combining reducers
const rootReducer = combineReducers({
  user: userReducer,
  categories: categoriesReducer,
  pokemons: pokemonsReducer,
});

// Configuring store
const configureStore = () => createStore(rootReducer, applyMiddleware(thunk));

export default configureStore;
