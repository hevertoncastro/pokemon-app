// import styled from 'styled-components/native';
// import colors from '../../settings/colors';
// import fonts from '../../settings/fonts';
// import {
//   widthPercentageToDP as wp,
//   heightPercentageToDP as hp,
// } from 'react-native-responsive-screen';

// export const ModalOverlay = styled.View`
//   flex: 1;
//   width: 100%;
//   justify-content: center;
//   align-items: center;
//   background: rgba(0, 0, 0, 0.5);
// `;

// export const ModalContent = styled.View`
//   justify-content: center;
//   align-items: center;
//   position: relative;
//   width: ${wp(90)};
//   background: ${colors.white};
//   border-radius: 10;
//   padding-top: ${wp(8)};
//   padding-bottom: ${wp(8)};
//   padding-left: ${wp(8)};
//   padding-right: ${wp(8)};

//   ${({ type }) =>
//     type &&
//     `
//     padding-top: ${wp(13)};
//   `}

//   ${({ height }) =>
//     height &&
//     `
//     height: ${wp(height)};
//   `}
// `;

// export const ModalIconHolder = styled.View`
//   justify-content: center;
//   align-items: center;
//   position: absolute;
//   top: ${-wp(10)};
//   width: ${wp(20)};
//   height: ${wp(20)};
//   border-radius: ${wp(10)};
//   background: ${colors.white};
// `;

// export const ModalIconWrap = styled.View`
//   justify-content: center;
//   align-items: center;
//   width: ${wp(18)};
//   height: ${wp(18)};
//   border-radius: ${wp(9)};
//   background: ${colors.warning};

//   ${({ success }) =>
//     success &&
//     `
//     background: ${colors.success};
//   `}

//   ${({ warning }) =>
//     warning &&
//     `
//     background: ${colors.warning};
//   `}

//   ${({ danger }) =>
//     danger &&
//     `
//     background: ${colors.danger};
//   `}
// `;

// export const ModalIconImage = styled.Image`
//   width: ${wp(8)};
//   height: ${wp(8)};
// `;

// export const ModalTitle = styled.Text`
//   justify-content: center;
//   align-self: center;
//   text-align: center;
//   font-size: ${wp(7)};
//   font-family: '${fonts.ExoMedium}';
//   color: ${colors.dark};
//   margin-bottom: ${wp(5)};
// `;

// export const ModalText = styled.Text`
//   justify-content: center;
//   align-self: center;
//   font-size: ${wp(4.5)};
//   text-align: center;
//   font-family: '${fonts.ExoRegular}';
//   color: ${colors.lightDark};
//   line-height: ${wp(5.5)};
//   margin-bottom: ${wp(6)};
// `;

// export const ModalTextHighlight = styled.Text`
//   font-family: '${fonts.ExoMedium}';
//   color: ${colors.main};
// `;

// export const ButtonsHolder = styled.View`
//   flex: 1;
//   flex-direction: row;
//   justify-content: space-between;
//   width: 80%;
// `;

// export const ModalButton = styled.TouchableOpacity`
//   justify-content: center;
//   align-self: center;
//   width: ${wp(50)};
//   padding-top: ${wp(3.5)};
//   padding-bottom: ${wp(3.5)};
//   border-radius: 30px;

//   ${({ highlight }) =>
//     highlight &&
//     `
//     background: ${colors.main}
//   `}

//   ${({ success }) =>
//     success &&
//     `
//     background: ${colors.success};
//   `}

//   ${({ warning }) =>
//     warning &&
//     `
//     background: ${colors.warning};
//   `}

//   ${({ danger }) =>
//     danger &&
//     `
//     background: ${colors.danger};
//   `}

//   ${({ small }) =>
//     small &&
//     `
//     width: 40%;
//   `}
// `;

// export const ModalButtonText = styled.Text`
//   justify-content: center;
//   align-self: center;
//   font-size: ${wp(5)};
//   font-family: '${fonts.ExoMedium}';
//   color: ${colors.softDark};

//   ${({ light }) =>
//     light &&
//     `
//     color: ${colors.white};
//   `}

//   ${({ underline }) =>
//     underline &&
//     `
//     text-decoration: underline;
//   `}
// `;

// export const CloseModalButton = styled.TouchableOpacity`
//   position: absolute;
//   top: 10px;
//   right: 5px;
//   margin-right: ${wp(3)};
// `;

// export const CloseModalButtonIcon = styled.Image`
//   width: ${wp(7)};
//   height: ${wp(7)};
// `;
