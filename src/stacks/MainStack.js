import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import PreLoad from '../screens/PreLoad';
import UserInput from '../screens/UserInput';
import CategoryPicker from '../screens/CategoryPicker';
import PokemonsList from '../screens/PokemonsList';
import PokemonDetails from '../screens/PokemonDetails';

const Stack = createStackNavigator();

export default () => (
  <Stack.Navigator
    initialRouteName="PreLoad"
    screenOptions={{ headerShown: false }}>
    <Stack.Screen name="PreLoad" component={PreLoad} />
    <Stack.Screen name="UserInput" component={UserInput} />
    <Stack.Screen name="CategoryPicker" component={CategoryPicker} />
    <Stack.Screen name="PokemonsList" component={PokemonsList} />
    <Stack.Screen name="PokemonDetails" component={PokemonDetails} />
  </Stack.Navigator>
);
