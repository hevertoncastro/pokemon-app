import { RECEIVE_CATEGORIES, SELECT_CATEGORY } from '../actions/types';

const initialState = {
  categories: [],
  selectedCategory: null,
};

const categoriesReducer = (state = initialState, action) => {
  switch (action.type) {
    case RECEIVE_CATEGORIES:
      return {
        ...state,
        categories: state.categories.concat(action.categories),
      };
    case SELECT_CATEGORY:
      return {
        ...state,
        selectedCategory: action.categoryName,
      };
    default:
      return state;
  }
};

export default categoriesReducer;
