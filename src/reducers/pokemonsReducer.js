import {
  RECEIVE_POKEMONS,
  FILTER_POKEMONS,
  SEARCH_POKEMONS,
  SORT_POKEMONS,
  SET_FAVORITE_POKEMONS,
  ADD_FAVORITE_POKEMON,
  REMOVE_FAVORITE_POKEMON,
} from '../actions/types';

import { sortList } from '../utils/sortList';

const initialState = {
  pokemons: [],
  filteredPokemons: [],
  favoritePokemons: [],
  sortOrder: 'asc',
};

import config from '../settings/config';

const pokemonsReducer = (state = initialState, action) => {
  switch (action.type) {
    case RECEIVE_POKEMONS:
      return {
        ...state,
        pokemons: state.pokemons.concat(action.pokemons),
      };
    case FILTER_POKEMONS: {
      let filtered = [];
      if (action.categoryName === config.FAVORITE.TEXT) {
        filtered = state.pokemons
          .filter((pokemon) => state.favoritePokemons.includes(pokemon.id))
          .sort(sortList('name', state.sortOrder));
      } else {
        filtered = state.pokemons
          .filter((pokemon) => pokemon.type.includes(action.categoryName))
          .sort(sortList('name', state.sortOrder));
      }
      return {
        ...state,
        filteredPokemons: [...filtered],
      };
    }
    case SEARCH_POKEMONS:
      return {
        ...state,
        filteredPokemons: state.filteredPokemons
          .filter((pokemon) =>
            pokemon.name
              .toLowerCase()
              .includes(action.searchQuery.toLowerCase()),
          )
          .sort(sortList('name', state.sortOrder)),
      };
    case SORT_POKEMONS:
      return {
        ...state,
        sortOrder: action.sortOrder,
        filteredPokemons: [
          ...state.filteredPokemons.sort(sortList('name', action.sortOrder)),
        ],
      };
    case SET_FAVORITE_POKEMONS:
      return {
        ...state,
        favoritePokemons: [...action.favoritePokemons],
      };
    case ADD_FAVORITE_POKEMON: {
      let idAlreadyExists =
        state.favoritePokemons.indexOf(action.pokemonId) > -1;
      let updatedFavorites = state.favoritePokemons.slice();

      if (idAlreadyExists) {
        updatedFavorites = updatedFavorites.filter(
          (id) => id !== action.pokemonId,
        );
      } else {
        updatedFavorites.push(action.pokemonId);
      }

      return {
        ...state,
        favoritePokemons: updatedFavorites,
      };
    }
    case REMOVE_FAVORITE_POKEMON:
      return {
        ...state,
        favoritePokemons: state.favoritePokemons.filter(
          (item) => item !== action.pokemonId,
        ),
      };
    default:
      return state;
  }
};

export default pokemonsReducer;
