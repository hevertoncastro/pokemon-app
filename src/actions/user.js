import { SET_USER_NAME } from './types';

export const setUserName = (user) => ({
  type: SET_USER_NAME,
  payload: user,
});
