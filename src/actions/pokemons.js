import fetch from 'cross-fetch';
import {
  REQUEST_POKEMONS,
  RECEIVE_POKEMONS,
  FILTER_POKEMONS,
  SEARCH_POKEMONS,
  SORT_POKEMONS,
  SET_FAVORITE_POKEMONS,
  ADD_FAVORITE_POKEMON,
  REMOVE_FAVORITE_POKEMON,
} from './types';

import config from '../settings/config';

function requestPokemons() {
  return {
    type: REQUEST_POKEMONS,
  };
}

function receivePokemons(pokemons) {
  return {
    type: RECEIVE_POKEMONS,
    pokemons,
  };
}

function getUniquePokemons(response) {
  const uniquePokemons = new Map();
  response.reduce(
    (prev, current) => prev.set(current.id, current),
    uniquePokemons,
  );

  return Array.from(uniquePokemons.values());
}

export function fetchPokemons() {
  return function (dispatch) {
    dispatch(requestPokemons());

    return fetch(`${config.API.BASE_URL}/${config.API.POKEMONS}`)
      .then((response) => response.json())
      .then((json) => dispatch(receivePokemons(getUniquePokemons(json))));
  };
}

export const filterPokemons = (categoryName) => ({
  type: FILTER_POKEMONS,
  categoryName,
});

export const searchPokemons = (searchQuery) => ({
  type: SEARCH_POKEMONS,
  searchQuery,
});

export const sortPokemons = (sortOrder) => ({
  type: SORT_POKEMONS,
  sortOrder,
});

export const setFavoritePokemons = (favoritePokemons) => ({
  type: SET_FAVORITE_POKEMONS,
  favoritePokemons,
});

export const addFavoritePokemon = (pokemonId) => ({
  type: ADD_FAVORITE_POKEMON,
  pokemonId,
});

export const removeFavoritePokemon = (pokemonId) => ({
  type: REMOVE_FAVORITE_POKEMON,
  pokemonId,
});
