import fetch from 'cross-fetch';
import {
  REQUEST_CATEGORIES,
  RECEIVE_CATEGORIES,
  SELECT_CATEGORY,
} from './types';

import config from '../settings/config';

function requestCategories() {
  return {
    type: REQUEST_CATEGORIES,
  };
}

function receiveCategories(categories) {
  const receivedCategoriesArray = categories;
  receivedCategoriesArray.unshift({
    thumbnailImage: config.FAVORITE.ICON_URL,
    name: config.FAVORITE.TEXT,
  });

  return {
    type: RECEIVE_CATEGORIES,
    categories: receivedCategoriesArray,
  };
}

export function fetchCategories() {
  return function (dispatch) {
    dispatch(requestCategories());

    return fetch(`${config.API.BASE_URL}/${config.API.TYPES}`)
      .then((response) => response.json())
      .then((json) => dispatch(receiveCategories(json.results)));
  };
}

export const selectCategory = (categoryName) => ({
  type: SELECT_CATEGORY,
  categoryName,
});
