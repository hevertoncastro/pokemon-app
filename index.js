/**
 * @format
 */

import 'react-native-gesture-handler';
import React from 'react';
import { AppRegistry, LogBox } from 'react-native';
import App from './App';
import { name as appName } from './app.json';
import { Provider } from 'react-redux';

import configureStore from './src/store';

// Ignore all log notifications:
LogBox.ignoreAllLogs();

const store = configureStore();

const PokemonFinder = () => (
  <Provider store={store}>
    <App />
  </Provider>
);

AppRegistry.registerComponent(appName, () => PokemonFinder);
