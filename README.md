## About the project

This app was developed for a position at [Vortigo](https://vortigo.digital/) using React Native (with React Native CLI), consuming data from Pokémon endpoints ([pokemons](https://vortigo.blob.core.windows.net/files/pokemon/data/pokemons.json), [types](https://vortigo.blob.core.windows.net/files/pokemon/data/types.json)).

##### Some of the libraries used

- **react-redux** (Used for global state management)
- **redux-thunk** (Used for async actions using Redux)
- **@react-navigation/native** (Used for screens navigation)
- **react-native-svg** (Used to easily handle SVG icons)
- **styled-components** (Used for styling with CSS syntax)
- **@react-native-community/async-storage** (Used for saving and retrieving data)

You may find more information of the libraries used in `package.json`.

#### Application summary



**1. Preload screen:**
This is just a presentation screen, that shows the app logo and a button that allows the user to use it.

![](docs/images/pokemon-app-01.png)

**2. User input screen:**
Now the user should put his/her name and press the button to go to next page.

![](docs/images/pokemon-app-02.png)

**3. Prefered type picker:**
The trainer must pick a Pokémon's prefered type, so this will be type selected to start to present the Pokémons.

![](docs/images/pokemon-app-03.png)

**4. List screen:**
This screen gets data from the endpoints ([pokemons](https://vortigo.blob.core.windows.net/files/pokemon/data/pokemons.json), [types](https://vortigo.blob.core.windows.net/files/pokemon/data/types.json)), and brings the Pokémons, which will then be displayed to the user.
The list shows an icon with the avatar of every pokémon as well as some extra info, e.g their abilities.
Also, there's a [favorite option](#favorite-your-pokemons) available.

![](docs/images/pokemon-app-04.png)

**5. Details screen:**
Shows the details of the pokémon. The user can see a larger avatar image, as well as the abilities, weakness, weight, and height; These work as tabs, and the user can access these tabs to find out more about the current Pokémon.
There's a button on the top right corner where the user can [favorite the current pokémon](#favorite-your-pokemons).

![](docs/images/pokemon-app-05.png)

**Some features:**

##### Favorite Your Pokemons:

The app offers an extension to the endpoints, which allows the user of the application to favorite his/her own pokémons!
There's an heart icon on both screens (List and Details), and when the user clicks on this icon the pokémon is then stored as one of his/her favorites.
This mechanism uses the user's cache/storage, and it is then synced/matched with the response from the API; therefore, when a new call is made to the API we still keep our favorite pokémons saved.
The user can see all pokémons favorited using the first button of the categories.

**Sorting Options**
The user is allowed to sort the list of pokémons by name, either on ascending order or descending order.

**Search**
The user may filter the pokémons displayed by their names.
For instance, if the user would like to find the pokémon "Pikachu" easily, he/she can simply type "Pik" on the search box with the electric category selected and hit the search icon.

---

## How to run the project

#### Installing dependencies
You will need Node, Watchman, the React Native command line interface, and Xcode.

While you can use any editor of your choice to develop your app, you will need to install Xcode in order to set up the necessary tooling to build your React Native app for iOS.

#### Node & Watchman
We recommend installing Node and Watchman using Homebrew. Run the following commands in a Terminal after installing Homebrew:

> brew install node
> brew install watchman

If you have already installed Node on your system, make sure it is Node **8.3** or newer.

Watchman is a tool by Facebook for watching changes in the filesystem. It is highly recommended you install it for better performance.

#### Packages and Libraries

Access the root directory and run the following command:

> yarn install

#### React Native Command Line Interface

React Native has a built-in command line interface. Rather than install and manage a specific version of the CLI globally, we recommend you access the current version at runtime using npx, which ships with Node.js. With npx react-native <command>, the current stable version of the CLI will be downloaded and executed at the time the command is run.


#### Running the application on iOS

> npx react-native run-ios

For Windows commands and further information, please access the [React Native's documentation page](https://reactnative.dev/docs/0.60/getting-started).


> If you can't get this to work, see the [Troubleshooting page](https://reactnative.dev/docs/0.60/troubleshooting#content).
